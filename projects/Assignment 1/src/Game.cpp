//Kaylyn McCune 100662337
#include "Game.h"

#include <stdexcept>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_glfw.h"

//for resizing the window
void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

//constructor definition
Game::Game() :
	myWindow(nullptr),
	myWindowTitle("Game"),
	myClearColor(glm::vec4(0.1f, 0.8f, 1.0f, 1.0f))
{ }

//destructor definition
Game::~Game() { }

//initialize the game
void Game::Initialize() {
	
	//Initialize GLFW
	if (glfwInit() == GLFW_FALSE)
	{
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}

	//enable transparent backbuffers for our windows (note that windows expects our colors to be pre-multiplied with alpha)
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

	//Create a new GLFW Window
	myWindow = glfwCreateWindow(600, 600, myWindowTitle, nullptr, nullptr);

	//we want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(myWindow);

	//let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0)
	{
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	//tie our game to our window, so we can access it via callbacks
	glfwSetWindowUserPointer(myWindow, this);
	//set our window resized callback
	glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);
}

//shutdown the game
void Game::Shutdown() {
	glfwTerminate();
}

void Game::LoadContent(){

	//create our 8 vertices
	//its a cube now
	Vertex vertices[8] =
	{
		//     Position                 Color
		// x       y     z       r     g     b     a
		{{0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f, 1.0f}},
		{{0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 0.0f, 1.0f}},
		{{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 1.0f, 1.0f}},
		{{-0.5f, 0.5f, 0.0}, {0.0f, 1.0f, 0.0f, 1.0f}},
		{{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},
		{{0.5f, -0.5f, -0.5f}, {1.0f, 1.0f, 0.0f, 1.0f}},
		{{-0.5f ,0.5f, -0.5f}, {1.0f, 0.0f, 1.0f, 1.0f}},
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}}
	};

	//create our 6 indices
	uint32_t indices[6] =
	{
		0, 1, 2,
		2, 1, 3
	};

	//create a new mesh from the data
	myMesh = std::make_shared<Mesh>(vertices, 8, indices, 6);

	//create and compile shader
	myShader = std::make_shared<Shader>();
	myShader->Load("passthrough.vs", "passthrough.fs");
}

void Game::UnloadContent() {

}

void Game::Update(float deltaTime) {

}

void Game::InitImGui() {
	
	//creates a new ImGUI context
	ImGui::CreateContext();

	//gets our ImGUI input/output
	ImGuiIO& io = ImGui::GetIO();

	//enable keyboard navigation
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

	//allow docking to our window
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

	//allow multiple viewports (so we can drag ImGui off our window)
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

	//allow our viewports to use transparent backbuffers
	io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

	//set up the ImGui implementation for open GL
	ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
	ImGui_ImplOpenGL3_Init("#version 410");

	//dark mode
	ImGui::StyleColorsDark();

	//get our imgui style
	ImGuiStyle& style = ImGui::GetStyle();

	//style.alpha = 1.0f
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 0.8f;
	}
}

void Game::ShutdownImGui() {
	
	//cleanup the ImGui implementation
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	
	//destroy our imgui context
	ImGui::DestroyContext();
}

void Game::ImGuiNewFrame() {
	
	//implementation new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();

	//imgui context new frame
	ImGui::NewFrame();
}

void Game::ImGuiEndFrame() {

	//make sure imgui knows how big our window is
	ImGuiIO& io = ImGui::GetIO();
	int width{ 0 }, height{ 0 };
	glfwGetWindowSize(myWindow, &width, &height);
	io.DisplaySize = ImVec2(width, height);

	//render all of our imgui elements
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	//if we have multiple viewports enabled (can drag into a new window)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		//update the windows that imgui is using
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		//restore our gl context
		glfwMakeContextCurrent(myWindow);
	}
}

void Game::Run() {
	
	Initialize();
	InitImGui();

	LoadContent();

	static float prevFrame = glfwGetTime();

	//run as long as the window is open
	while (!glfwWindowShouldClose(myWindow)) {
		
		//poll for events from windows
		//clicks, key presses, closing, ect
		glfwPollEvents();

		float thisFrame = glfwGetTime();
		float deltaTime = thisFrame - prevFrame;

		Update(deltaTime);
		Draw(deltaTime);

		ImGuiNewFrame();
		DrawGui(deltaTime);
		ImGuiEndFrame();

		prevFrame = thisFrame;
		//present our image to windows
		glfwSwapBuffers(myWindow);
	}

	UnloadContent();
	ShutdownImGui();
	Shutdown();
}

void Game::Draw(float deltaTime) {

	//clear our screen every frame
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT);

	myShader->Bind();
	myMesh->Draw();

}

void Game::DrawGui(float deltaTime) {

	//open a new imgui window
	ImGui::Begin("Test");

	//draw widgets here]

	//draw a color editor
	ImGui::ColorEdit4("Clear Color", &myClearColor[0]);

	//check if textbox has changed, and update window title if it has
	if (ImGui::InputText("Window Title", myWindowTitle, 32))
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);

	}

	ImGui::End();
}