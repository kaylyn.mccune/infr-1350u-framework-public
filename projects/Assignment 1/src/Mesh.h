#pragma once
//Kaylyn McCune- 100662337

#include <glad/glad.h>
#include <GLM/glm.hpp> //for vec3 and vec4
#include <cstdint> //needed for uint32_t
#include <memory> //needed for smart pointers

struct Vertex {
	glm::vec3 Position;
	glm::vec4 Color;
};

class Mesh {

public:
	//shorthand for shared_ptr
	typedef std::shared_ptr<Mesh> Sptr;

	//creates a new mesh from the given vertices and indices
	Mesh(Vertex* vertices, size_t numVerts, uint32_t* indices, size_t numIndices);
	~Mesh();

	void loadMesh();

	//draws this mesh
	void Draw();

private:
	
	//our gl handle for the vertex array object
	GLuint myVao;

	//0 is vertices, 1 is indices
	GLuint myBuffers[2];

	//the number of vertices and indices in this mesh
	size_t myVertexCount, myIndexCount;
};