//Kaylyn McCune- 100662337
#include "ReadFile.h"
#include <fstream>

//reads the entire contens of a file
char* readFile(const char* filename) {

	//declare and open the file stream
	std::ifstream file;
	file.open(filename, std::ios::binary);

	//only read if the file is open
	if (file.is_open()) {

		//get the starting location in the file
		uint64_t  fileSize = file.tellg();
		//seek to the end
		file.seekg(0, std::ios::end);
		//calculate the file size from end to beginning
		fileSize = (uint64_t)file.tellg() - fileSize;
		//seek back to the beginning of the file
		file.seekg(0, std::ios::beg);

		//allocate space for our entire file, +1 byte at the end for null terminator
		char* result = new char[fileSize + 1];
		//read the entire file to our memory
		file.read(result, fileSize);

		//make our text null-terminated
		result[fileSize] = '\0';

		//close the file before returning
		file.close();
		return result;
	}
	//otherwise, we failed to open our file, throw a runtime error
	else
	{
		throw std::runtime_error("We cannot open the file!");
	}
}