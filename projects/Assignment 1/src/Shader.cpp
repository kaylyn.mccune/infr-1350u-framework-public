//Kaylyn McCune- 100662337

#include "Shader.h"
#include "Logging.h"
#include "ReadFile.h"

Shader::Shader() {
	myShaderHandle = glCreateProgram();
}

Shader::~Shader() {
	glDeleteProgram(myShaderHandle);
}

void Shader::Bind() {
	glUseProgram(myShaderHandle);
}

GLuint Shader::__CompileShaderPart(const char* source, GLenum type) {
	GLuint result = glCreateShader(type);

	//load in our shader source and compile it
	glShaderSource(result, 1, &source, NULL);
	glCompileShader(result);

	//check our compile status
	GLint compileStatus = 0;
	glGetShaderiv(result, GL_COMPILE_STATUS, &compileStatus);

	//if we failed to compile
	if (compileStatus == GL_FALSE)
	{
		//get the size of the error log
		GLint logSize = 0;
		glGetShaderiv(result, GL_INFO_LOG_LENGTH, &logSize);

		//create a new character buffer for the log
		char* log = new char[logSize];

		//get the log
		glGetShaderInfoLog(result, logSize, &logSize, log);

		//dump error log
		LOG_ERROR("Failed to compile shader part:\n{}", log);

		//clean up our log memory
		delete[] log;

		//delete the broken shader result
		glDeleteShader(result);

		//throw a runtime exception
		throw new std::runtime_error("Failed to compile the shader part!");
	}
	else {
		LOG_TRACE("Shader part has been compiled!");
	}

	//return the compiled shader part
	return result;
}

void Shader::Compile(const char* vs_source, const char* fs_source) {

	//compile our two shader programs
	GLuint vs = __CompileShaderPart(vs_source, GL_VERTEX_SHADER);
	GLuint fs = __CompileShaderPart(fs_source, GL_FRAGMENT_SHADER);

	//attach our 2 shaders
	glAttachShader(myShaderHandle, vs);
	glAttachShader(myShaderHandle, fs);

	//perform linking
	glLinkProgram(myShaderHandle);

	//remove shader parts to save space
	glDetachShader(myShaderHandle, vs);
	glDeleteShader(vs);
	glDetachShader(myShaderHandle, fs);
	glDeleteShader(fs);

	//get whether link was successful
	GLint success = 0;
	glGetProgramiv(myShaderHandle, GL_LINK_STATUS, &success);

	//if not, we need to grab the log and throw an exception
	if (success == GL_FALSE)
	{
		//get the length of the log
		GLint length = 0;
		glGetProgramiv(myShaderHandle, GL_INFO_LOG_LENGTH, &length);

		if (length > 0)
		{
			//read the log from opengl
			char* log = new char[length];
			glGetProgramInfoLog(myShaderHandle, length, &length, log);
			LOG_ERROR("Shader failed to link:\n{}", log);
			delete[] log;
		}
		else
		{
			LOG_ERROR("Shader failed to link for an unknown reason!");
		}

		//delete the partial program
		glDeleteProgram(myShaderHandle);

		//throw a runtime exception
		throw new std::runtime_error("Failed to link shader program!");
	}
	else
	{
		LOG_TRACE("Shader has been linked");
	}
}

void Shader::Load(const char* vsFile, const char* fsFile) {

	//load in our shaders
	char* vs_source = readFile(vsFile);
	char* fs_source = readFile(fsFile);

	//compile our program
	Compile(vs_source, fs_source);

	//clean up our memory
	delete[] fs_source;
	delete[] vs_source;
}