#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Game.h"
#include "Logging.h"
//Kaylyn McCune- 100662337

int main() {

	Logger::Init();

	Game* game = new Game();
	game->Run();
	delete game;

	return 0;
}