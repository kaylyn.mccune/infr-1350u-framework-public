#version 410

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec4 inColor;
layout (location = 2) in vec3 inNormal;
// New in tutorial 06
layout (location = 3) in vec2 inUV;

layout (location = 0) out vec4 outColor;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec3 outWorldPos;
// New in tutorial 06
layout (location = 3) out vec2 outUV;

layout (location = 4) out vec3 outTexWeights;

uniform mat4 a_ModelViewProjection;
uniform mat4 a_Model;
uniform mat4 a_ModelView;
uniform mat3 a_NormalMatrix;

uniform sampler2D a_heightMap;

void main() {
	outColor = inColor;
	outNormal = a_NormalMatrix * inNormal;
	outColor = inColor;
	
	vec3 newHeight = inPosition; //setting a new height
	newHeight.z = texture(a_heightMap,inUV).r * 2; //new height based on the heightmap

	outWorldPos =  (a_Model * vec4(newHeight, 1)).xyz;
	gl_Position = a_ModelViewProjection * vec4(newHeight, 1);

	//vertex texture weighting

	float sand = 1.0f;
	float grass = 0.0f;
	float mountain = 0.0f;

	if( (newHeight.z/2) <= 0.4 ){
		sand = 1.0 - (newHeight.z/2);
		grass = 1.0f - sand;
	}

	if((newHeight.z/2) > 0.4){
		mountain = ((newHeight.z/2) - 0.4) * 2; 
		grass = 1.0f - mountain;
	}

	outTexWeights = vec3(
		sand, //x
		grass, //y
		mountain	//z
	);

	// New in tutorial 06
	//outUV = inUV;

	outUV = outWorldPos.xy;
}