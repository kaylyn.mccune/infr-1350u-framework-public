#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <functional>

#include "GLM/glm.hpp"

#include "Mesh.h"
#include "Shader.h"
#include "Camera.h"

class Game {
public:
	Game();
	~Game();

	void Run();

	void Resize(int newWidth, int newHeight);

protected:
	void Initialize();
	void Shutdown();

	void LoadContent();
	void UnloadContent();

	void InitImGui();
	void ShutdownImGui();

	void ImGuiNewFrame();
	void ImGuiEndFrame();

	void Update(float deltaTime);
	void Draw(float deltaTime);
	void DrawGui(float deltaTime);

	//new in tutuorial 10
	glm::ivec2  myWindowSize;
	void __RenderScene(glm::ivec4 viewport, Camera::Sptr camera);

private:
	// Stores the main window that the game is running in
	GLFWwindow* myWindow;
	// Stores the clear color of the game's window
	glm::vec4   myClearColor;
	// Stores the title of the game's window
	char        myWindowTitle[32];

	//all the cameras
	Camera::Sptr myCamera;
	Camera::Sptr myCamera2;
	Camera::Sptr myCamera3;
	Camera::Sptr myCamera4;

	std::vector<Camera::Sptr> cameras;

	int activeCamera = 0;

	bool delay = false;
	float delayTime = 0.5;

	// Our models transformation matrix
	glm::mat4   myModelTransform;
};