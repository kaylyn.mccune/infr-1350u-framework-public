#pragma once
//Kaylyn McCune - 100662337

#include "Material.h"
#include "Mesh.h"

struct MeshRenderer {
	Material::Sptr Material;
	Mesh_sptr Mesh;
};