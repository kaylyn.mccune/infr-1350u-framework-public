#include "Texture2D.h"
#include "Logging.h"
#include <stb_image.h>

Texture2D::Texture2D(const Texture2DDescription& desc) {
	myDescription = desc;

	myTextureHandle = 0;
	__SetupTexture();
}

Texture2D::~Texture2D() {
	glDeleteTextures(1, &myTextureHandle);
}

void Texture2D::__SetupTexture() {
	glCreateTextures(GL_TEXTURE_2D, 1, &myTextureHandle);
	glTextureStorage2D(myTextureHandle, 1,
		(GLenum)myDescription.Format, myDescription.Width, myDescription.Height);
}

void Texture2D::Bind(int slot) const {
	//bind the given texture slot, opengl 4 guarantees that we have at least 80 texture slots
	//note that this is part of direct state access added in 4.5 replacing the old glactivetexture and glbindtexture calls
	glBindTextureUnit(slot, myTextureHandle);
}

void Texture2D::UnBind(int slot) {
	//binding zero to a texture slot will unbind the texture
	glBindTextureUnit(slot, 0);
}