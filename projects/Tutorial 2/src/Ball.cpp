//Kaylyn McCune- 100662337
//Spencer Tester- 100653129

#include "Ball.h"

//constructor/destructor
Ball::Ball() {
	life = 3;
	//set position to the same as the mesh for the ball
	velocity = glm::vec3(0, 1, 1);
}

Ball::~Ball() {

}

//Get/Set Functions
glm::vec3 Ball::GetPosition() {
	return position;
}
void Ball::SetPosition(glm::vec3 p) {
	position = p;
}

glm::vec3 Ball::GetVelocity() {
	return velocity;
}
void Ball::SetVelocity(glm::vec3 v) {
	velocity = v;
}

int Ball::GetLife() {
	return life;
}
void Ball::SetLife(int l) {
	life = l;
}

//Reverses the velocity vector if the ball bounces
void Ball::Bounce(int side) {
	//1 = left/right
	//2 = up/down
	//3 = both
	if (side == 1) {
		velocity = glm::vec3(velocity.x, -velocity.y, velocity.z);
	}
	else if (side == 2) {
		velocity = glm::vec3(velocity.x, velocity.y, -velocity.z);
	}
	else {
		velocity = glm::vec3(velocity.x, -velocity.y, -velocity.z);
	}
}

//Reduces the players life by one if the ball goes off screen
void Ball::LoseLife() {
	life -= 1;
}

//updates the player's position based on the velocity vector
void Ball::UpdatePosition() {
	position += velocity;
}