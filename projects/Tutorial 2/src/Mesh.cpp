//Kaylyn McCune- 100662337

#include <glad/glad.h>
#include <GLM/glm.hpp> //for vec3 and vec4
#include <cstdint> //needed for uint32_t
#include <memory> //needed for smart pointers
#include "Mesh.h"

Mesh::~Mesh() {

	//clean up our buffers
	glDeleteBuffers(2, myBuffers);

	//clean up our VAO
	glDeleteVertexArrays(1, &myVao);
}

Mesh::Mesh(Vertex* vertices, size_t numVerts, uint32_t* indices, size_t numIndices) {

	myIndexCount = numIndices;
	myVertexCount = numVerts;

	//create and bind our vertex array
	glCreateVertexArrays(1, &myVao);
	glBindVertexArray(myVao);

	//create 2 buffers, 1 for vertices and the other for indices
	glCreateBuffers(2, myBuffers);

	//bind and buffer our vertex data
	glBindBuffer(GL_ARRAY_BUFFER, myBuffers[0]);
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	//bind and buffer our index data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myBuffers[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(uint32_t), indices, GL_STATIC_DRAW);

	//get a null vertex to get member offsets from
	Vertex* vert = nullptr;

	//enable vertex attribute 0
	glEnableVertexAttribArray(0);
	//our first attribute is 3 floats, the distance between
	//them is the size of our vertex, and they will map the position in our vertices
	glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), &(vert->Position));

	//enable vertex attribute 1
	glEnableVertexAttribArray(1);
	//our second attribute is 4 floats, the distance between
	//them is the size of our vertex, and they will map to the color in our vertices
	glVertexAttribPointer(1, 4, GL_FLOAT, false, sizeof(Vertex), &(vert->Color));

	//unbind our voa
	glBindVertexArray(0);
}

void Mesh::Draw() {

	//bind the mesh
	glBindVertexArray(myVao);
	//draw all of our vertices as triangles, our indexes are unsigned ints (uint32_t)
	glDrawElements(GL_TRIANGLES, myIndexCount, GL_UNSIGNED_INT, nullptr);
}