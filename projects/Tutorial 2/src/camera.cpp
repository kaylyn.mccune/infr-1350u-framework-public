//Kaylyn McCune- 100662337

#include "Camera.h"

#include <GLM/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/gtx/quaternion.hpp>

Camera::Camera() :
	myPosition(glm::vec3(0)),
	myView(glm::mat4(1.0f)),
	Projection(glm::mat4(1.0f))
{ }

Camera::~Camera() {

}

void Camera::SetPosition(const glm::vec3& pos) {
	myView[3] = glm::vec4(-(glm::mat3(myView) * pos), 1.0f);
	myPosition = pos;
}

void Camera::LookAt(const glm::vec3& target, const glm::vec3& up) {
	myView = glm::lookAt(myPosition, target, up);
}

void Camera::Rotate(const glm::quat& rot) {
	//only update if we have an actual value to rotate by
	if (rot != glm::quat(glm::vec3(0)))
	{
		myView = glm::mat4_cast(rot) * myView;
	}
}

void Camera::Move(const glm::vec3& local) {
	//only update if we have actually moved
	if (local != glm::vec3(0)) {
		//we only need to subtract since we are already in thr camera's local space
		myView[3] -= glm::vec4(local, 0);
		//recalculate our position in the world space and cache it
		myPosition = -glm::inverse(glm::mat3(myView)) * myView[3];
	}
}